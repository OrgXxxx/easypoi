package xyz.a51lzz.service;

import xyz.a51lzz.domain.entity.User;

import java.util.List;

/**
 * Created by lzz on 5/8/17.
 */
public interface IUserService {

    List<User> getList();

    User save(User user);

    void save(List<User> list);

}
